package com.example.myflarebeta;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class Main2Activity extends AppCompatActivity {

//    private TextView mTextMessage;
    public BottomNavigationView navigation;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            FragmentManager mfragmentManager = getSupportFragmentManager();

            switch (item.getItemId()) {
                case R.id.navigation_flare:

                    if (mfragmentManager != null) {
                        FlareFragment flareFragment = new FlareFragment();
                        FragmentTransaction mFragmentTransaction = mfragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.frame_container, flareFragment, FlareFragment.class.getSimpleName());
                        mFragmentTransaction.commit();
                    }
                    return true;

                case R.id.navigation_call:

                    if (mfragmentManager != null) {
                        CallFragment callFragment = new CallFragment();
                        FragmentTransaction mFragmentTransaction = mfragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.frame_container, callFragment, CallFragment.class.getSimpleName());
                        mFragmentTransaction.commit();
                    }
                    return true;

                case R.id.navigation_profile:

                    return true;


                case R.id.navigation_maps:
//                    Fragment fragment = mfragmentManager.findFragmentByTag(FlareFragment.class.getSimpleName());

//                    if (!(fragment instanceof MapsFragment)) {
                        if (navigation.getSelectedItemId() != R.id.navigation_maps) {
                            if (mfragmentManager != null) {
                                MapsFragment mapsFragment = new MapsFragment();
                                FragmentTransaction mFragmentTransaction = mfragmentManager.beginTransaction();
                                mFragmentTransaction.replace(R.id.frame_container, mapsFragment, MapsFragment.class.getSimpleName());
                                mFragmentTransaction.commit();
                            }
                        }
//                    }
//
//                    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                            .findFragmentById(R.id.map);
//                    mapFragment.getMapAsync(this);
                    return true;

            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

//        mTextMessage = (TextView) findViewById(R.id.message);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//
//        FragmentManager mFragmentManager = getSupportFragmentManager();
//        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
//
//        FlareFragment flareFragment = new FlareFragment();
//
//
//        Fragment fragment = mFragmentManager.findFragmentByTag(FlareFragment.class.getSimpleName());

//        if (!(fragment instanceof FlareFragment)) {
//            mFragmentTransaction.add(R.id.frame_container, flareFragment, FlareFragment.class.getSimpleName());
//            Log.d("MyFlexibleFragment", "Fragment Name :" + FlareFragment.class.getSimpleName());
//            mFragmentTransaction.commit();
//        }

        if (savedInstanceState == null) {
            navigation.setSelectedItemId(R.id.navigation_flare);
        }

//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setLogo(R.drawable.suluhs);
//        getSupportActionBar().setDisplayUseLogoEnabled(true);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().hide();
    }

    public void setNavigationMaps(){
        navigation.setSelectedItemId(R.id.navigation_maps);
    }

}
