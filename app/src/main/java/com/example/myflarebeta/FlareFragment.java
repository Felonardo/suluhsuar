package com.example.myflarebeta;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class FlareFragment extends Fragment implements View.OnClickListener {

    int drawablePath;
    String iconId="1";


    RadioGroup rgIcon, rgReceiver;
    RadioButton rb1;
    EditText edtMessage;
    Button btnSendflare;

    private String userID;
    private FirebaseAuth mAuth;
    private DatabaseReference Database;

    public FlareFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_flare, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnSendflare = view.findViewById(R.id.btn_sendFlare);

        btnSendflare.setOnClickListener(this);

        edtMessage = view.findViewById(R.id.edt_message);
        rgIcon = view.findViewById(R.id.rg_icon);
//        rgReceiver = view.findViewById(R.id.rg_receiver);
        rb1=view.findViewById(R.id.rb1);

        rgIcon.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                boolean isChecked = checkedRadioButton.isChecked();
//                rgIcon.check(R.id.rb1);
                if (isChecked) {
//                    rgIcon.check(R.id.rb1);
                    switch (rgIcon.getCheckedRadioButtonId()) {
                        case R.id.rb1:
                            Toast.makeText(getActivity(), getString(R.string.robbery), Toast.LENGTH_SHORT).show();

                            iconId="1";
//                            drawablePath = R.mipmap.flare2;

                            break;
                        case R.id.rb2:
                            Toast.makeText(getActivity(), getString(R.string.medicc), Toast.LENGTH_SHORT).show();

                            iconId="2";
//                            drawablePath = R.mipmap.flare1;

                            break;

                        case R.id.rb3:
                            Toast.makeText(getActivity(), getString(R.string.firec), Toast.LENGTH_SHORT).show();

                            iconId="3";
//                            drawablePath = R.mipmap.flare1;

                            break;

                        case R.id.rb4:
                            Toast.makeText(getActivity(), getString(R.string.disasterc), Toast.LENGTH_SHORT).show();

                            iconId="4";
//                            drawablePath = R.mipmap.flare1;

                            break;



                        default:
                            Toast.makeText(getActivity(), "you choose default", Toast.LENGTH_SHORT).show();

                            iconId="1";
//                            drawablePath = R.mipmap.flare1;
                            break;
                    }
                }
            }
        });


    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_sendFlare) {
            //TODO send flare dan intent ke maps

            String message = edtMessage.getText().toString().trim();


            Toast.makeText(getActivity(), "flare telah tekirim " + message, Toast.LENGTH_SHORT).show();

//
//            Intent intent = new Intent(getActivity().getBaseContext(),
//                    Main2.class);
//            intent.putExtra("message", message);
//            getActivity().startActivity(intent);
//
//


            mAuth = FirebaseAuth.getInstance();
            userID = mAuth.getCurrentUser().getUid();
            Database = FirebaseDatabase.getInstance().getReference().child("Users").child("Public").child(userID);

            Map userInfo = new HashMap();
            userInfo.put("title", message);
//            userInfo.put("icon", drawablePath);
            userInfo.put("iconid",iconId);
            Database.updateChildren(userInfo);

            MapsFragment fragment = new MapsFragment();

            Bundle i = new Bundle();
            i.putString("message", message);
            i.putString("icon", iconId);
//            i.putInt("iconId",iconId);

            fragment.setArguments(i);

            getFragmentManager().beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .addToBackStack(null)
                    .commit();



//            ((Main2Activity)getActivity()).setNavigationMaps();

//            ((Main2Activity)getActivity())navigation.setSelectedItemId(R.id.navigation_flare);

//            // Create new fragment and transaction
//            Fragment newFragment = new MapsFragment();
//            // consider using Java coding conventions (upper first char class names!!!)
//            FragmentTransaction transaction = getFragmentManager().beginTransaction();
//
//            // Replace whatever is in the fragment_container view with this fragment,
//            // and add the transaction to the back stack
//            transaction.replace(R.id.frame_container, newFragment);
//            transaction.addToBackStack(null);
//
//            // Commit the transaction
//            transaction.commit();

        }
    }

}
