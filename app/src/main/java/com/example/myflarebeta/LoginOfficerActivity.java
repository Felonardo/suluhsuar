package com.example.myflarebeta;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class LoginOfficerActivity extends AppCompatActivity {

    private EditText password;
    private Button login, register;
    private AutoCompleteTextView email;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener authStateListener;

//    private String statusUsers;
//    private boolean isPublic = false;


    RadioGroup rgOptions;
    RadioButton rbPublic, rbPolice, rbHospital, rbFireforce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_officer2);

        //initialize radio group

//        rgOptions = findViewById(R.id.rg_option);
//        rgOptions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
//                boolean isChecked = checkedRadioButton.isChecked();
//                if (isChecked) {
//                    Toast.makeText(LoginOfficerActivity.this, "you will login as a " + checkedRadioButton.getText(), Toast.LENGTH_SHORT).show();
//                    statusUsers = checkedRadioButton.getText().toString().trim();
//                    isPublic=true;
//                }
//            }
//        });


        mAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    Intent intent = new Intent(LoginOfficerActivity.this, Main2Activity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };


        email = (AutoCompleteTextView) findViewById(R.id.email);
        login = (Button) findViewById(R.id.email_sign_in_button);
        password = (EditText) findViewById(R.id.password);
        register = (Button) findViewById(R.id.email_register_button);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String semail = email.getText().toString();
                final String spassword = password.getText().toString();
                mAuth.createUserWithEmailAndPassword(semail, spassword).addOnCompleteListener(LoginOfficerActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(LoginOfficerActivity.this, "sign up error", Toast.LENGTH_SHORT).show();
                        } else {
//                            if (isPublic == false) {
//                                String user_id = mAuth.getCurrentUser().getUid();
//                                DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("Users").child("Officer").child(statusUsers).child(user_id);
//                                current_user_db.setValue(true);
//                            } else {
                                String user_id = mAuth.getCurrentUser().getUid();
                                DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("Users").child("Public").child(user_id);
                                current_user_db.setValue(true);
//                            }
                        }
                    }
                });
            }
        });

        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final String semail = email.getText().toString();
                final String spassword = password.getText().toString();
                mAuth.signInWithEmailAndPassword(semail, spassword).addOnCompleteListener(LoginOfficerActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(LoginOfficerActivity.this, "sign in error", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }

        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(authStateListener);

    }

}