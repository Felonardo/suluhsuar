package com.example.myflarebeta;


import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapsFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, View.OnClickListener {

    DatabaseReference titleDatabase;
    String myTitle;
    String myIconId;
    Button btnRefresh;

    Marker mOfficerMarker;
    View mView;
    MapView mMapView;
    private GoogleMap mMap;
    private Marker marker;
    LatLng latLng;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;

    LocationRequest mLocationRequest;

    public MapsFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
//        titleDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Public").child(userID);
//        getTitle();
//        Bundle i=this.getArguments();
//        if(i != null){
//            titlex = i.getString("name","");
//        }
//        title="oke";
//        Toast.makeText(getActivity(),"ini "+title,Toast.LENGTH_SHORT).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        Bundle i = this.getArguments();
        if (i != null) {
            myTitle = i.getString("message", "");
            myIconId = i.getString("icon", "");
        }
//        Toast.makeText(getActivity(),"ini "+title,Toast.LENGTH_SHORT).show();

        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_maps, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnRefresh = view.findViewById(R.id.btn_refresh);
        btnRefresh.setOnClickListener(this);

        mMapView = (MapView) mView.findViewById(R.id.mapss);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (marker != null) {
            marker.remove();
        }
        mLastLocation = location;

        latLng = new LatLng(location.getLatitude(), location.getLongitude());


        //how to save location to firebase
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("OfficerAvailable");

        GeoFire geoFire = new GeoFire(ref);
        geoFire.setLocation(userId, new GeoLocation(location.getLatitude(), location.getLongitude()));

        getOfficerAround();

//        if (!getOfficerAroundStarted) {
//
//            getOfficerAround();
//
//
//        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        buildGoogleApiClient();

        MapsInitializer.initialize(getContext());
        mMap = googleMap;

//        getOfficerAround();

        mMap.setPadding(10, 180, 10, 10);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            if (!mMap.isMyLocationEnabled())
                mMap.setMyLocationEnabled(true);

            LocationManager lm = (LocationManager) this.getContext().getSystemService(Context.LOCATION_SERVICE);
            mLastLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (mLastLocation == null) {
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                String provider = lm.getBestProvider(criteria, true);
                mLastLocation = lm.getLastKnownLocation(provider);
            }

            if (mLastLocation != null) {
                latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14), 1500, null);
            }
        }


        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

    }


    String iconId;

    List<Marker> markers = new ArrayList<Marker>();
    Boolean getOfficerAroundStarted = false;

    private void getOfficerAround() {
        getOfficerAroundStarted = true;
        try {
            final String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
//            titleDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Public").child(userID);
//            getTitle();

            DatabaseReference officerLocation = FirebaseDatabase.getInstance().getReference().child("OfficerAvailable");
            GeoFire geoFire = new GeoFire(officerLocation);

            GeoQuery geoQuery = geoFire.queryAtLocation(new GeoLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude()), 1000000000);

//            Toast.makeText(MapsActivity.this,"ok1",Toast.LENGTH_SHORT).show();
            String key = officerLocation.getKey();

            geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
                @Override
                public void onKeyEntered(String key, GeoLocation location) {


//                    getTitle();
//                    getIcon();

//                    Toast.makeText(getActivity(), "ok1 = " + key, Toast.LENGTH_SHORT).show();
//                    Toast.makeText(getActivity(), "ok1 = " + titlex, Toast.LENGTH_SHORT).show();
//                    Toast.makeText(getActivity(), "ok2 = " + iconId, Toast.LENGTH_SHORT).show();

                    for (Marker i : markers) {
                        if (i.getTag().equals(key)) {

                            return;
                        }
                    }

                    try {

                        LatLng officerLocation = new LatLng(location.latitude, location.longitude);
//                        if (key != userID) {

//                        Toast.makeText(getActivity(), iconId, Toast.LENGTH_SHORT).show();
//                            if (iconId == "1") {
//                                mOfficerMarker = mMap.addMarker(new MarkerOptions().position(officerLocation).title(titlex).icon(BitmapDescriptorFactory.fromResource(R.mipmap.flare1)));
//                            } else if (iconId == "2") {
                        mOfficerMarker = mMap.addMarker(new MarkerOptions().position(officerLocation).title(key));
//                            }
//                            mOfficerMarker.setTag(key);
//                            markers.add(mOfficerMarker);
//                        } else if (key == userID && myTitle != null) {
//                            if (myIconId == "1") {
//                                mOfficerMarker = mMap.addMarker(new MarkerOptions().position(officerLocation).title(myIconId+myTitle).icon(BitmapDescriptorFactory.fromResource(R.mipmap.flare1)));
//                            } else if (myIconId == "2") {
//                                mOfficerMarker = mMap.addMarker(new MarkerOptions().position(officerLocation).title(myIconId+myTitle).icon(BitmapDescriptorFactory.fromResource(R.mipmap.flare2)));
//                            }


                        mOfficerMarker.setTag(key);
                        markers.add(mOfficerMarker);
//                        }
                        refresh();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                @Override
                public void onKeyExited(String key) {

                    for (Marker i : markers) {
                        if (i.getTag().equals(key)) {
                            i.remove();
                            markers.remove(i);
                            return;
                        }
                    }

                }

                @Override
                public void onKeyMoved(String key, GeoLocation location) {

                    for (Marker i : markers) {
                        if (i.getTag().equals(key)) {
                            i.setPosition(new LatLng(location.latitude, location.longitude));
                            refresh();
//                            if (key != userID) {
//                                titleDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Public").child(key);
//                                getTitle();
//                                i.setTitle(iconId+titlex);
//                                getIcon();
//                                Toast.makeText(getActivity(),"icon: "+iconId,Toast.LENGTH_SHORT).show();
//                                if (iconId == "1") {
//                                    i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.flare1));
//                                } else if (iconId == "2") {
//                                    i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.flare2));
//                                }
//                            }
//                            if (key == userID && myTitle != null) {
//                                i.setTitle(myIconId+myTitle);

//                                if (myIconId == "1") {
//                                    i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.flare1));
//                                } else if (myIconId == "2") {
//                                    i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.flare2));
//                                }

//                            }
                        }
//                        if (i.getTag().equals(key) && key== userID) {
//                            i.setPosition(new LatLng(location.latitude, location.longitude));
//
//                            titleDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Public").child(key);
//                            getTitle();
//                            i.setTitle(titlex);
//                        }
                    }

                }

                @Override
                public void onGeoQueryReady() {

                }

                @Override
                public void onGeoQueryError(DatabaseError error) {

                }
            });
        } catch (Exception e) {

        }
    }

//    List<String> titles = new ArrayList<String>();

    String titlex;

    private void getTitle() {

    }

    private void getIcon() {
        titleDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0) {
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    if (map.get("iconid") != null) {
                        iconId = map.get("iconid").toString();
//                        Toast.makeText(getActivity(), "ini" + title, Toast.LENGTH_SHORT).show();

//                        for (String i : titles) {
//                            titles.add(map.get("title").toString());
//                        }
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

//    int iconId;
//    private void getIconId(){
//        titleDatabase.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0) {
//                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
//                    if (map.get("icon") != null) {
////                        iconId = (int) map.get("icon");
////                        realIconId = (int) map.get("iconId");
////                        Toast.makeText(getActivity(), "ini" + title, Toast.LENGTH_SHORT).show();
//
////                        for (String i : titles) {
////                            titles.add(map.get("title").toString());
////                        }
//                    }
//
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (LocationListener) this);
            String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("OfficerAvailable");

            GeoFire geoFire = new GeoFire(ref);
            geoFire.removeLocation(userId);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_refresh) {
//            getOfficerAround();
            Toast.makeText(getActivity(), "oke", Toast.LENGTH_SHORT).show();
            refresh();

//                if (i.getTitle().charAt(0) == 'g') {
//                    i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.flare1));
//                } else if (i.getTitle().charAt(0) == '2') {
//                    i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.flare2));
//                }
//                markers.add(mOfficerMarker);

        }
    }

    public void refresh() {
        for (final Marker i : markers) {
            titleDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Public").child((String) i.getTag());
//            getTitle();
//            getIcon();

            titleDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0) {
                        Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                        if (map.get("title") != null && map.get("iconid") != null) {
                            if (map.get("iconid").toString() == "1") {
                                i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.pinperampokan));
                            } else if (map.get("iconid").toString() == "2") {
                                i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.pinmedis));
                            } else if (map.get("iconid").toString() == "3") {
                                i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.pinkebakaran));
                            } else if (map.get("iconid").toString() == "4") {
                                i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.pinbencana));
                            }



                            i.setTitle(map.get("iconid").toString()+map.get("title").toString());
//                            Toast.makeText(getActivity(), i.getTag() + "titleinull" + map.get("title"), Toast.LENGTH_SHORT).show();
//
//                            Toast.makeText(getActivity(), i.getTag()+"cek title" + map.get("title"), Toast.LENGTH_SHORT).show();
//                            Toast.makeText(getActivity(), i.getTag()+"cek iconid" + map.get("iconid"), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getActivity(), "ini" + title, Toast.LENGTH_SHORT).show();

//                        for (String i : titles) {
//                            titles.add(map.get("title").toString());
//


                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
//
            if (i.getTitle().charAt(0) == '1') {
                i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.pinperampokan));
            }else if (i.getTitle().charAt(0) == '2') {
                i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.pinmedis));

            }else if (i.getTitle().charAt(0) == '3') {
                i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.pinkebakaran));

            }else if (i.getTitle().charAt(0) == '4') {
                i.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.pinbencana));

            }
//            String t =i.getTitle().substring(1);
//            i.setTitle(t);
//            i.setTitle(titlex);

        }
    }
}

