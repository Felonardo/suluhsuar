package com.example.myflarebeta;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class CallFragment extends Fragment implements View.OnClickListener {


    Button callPolice, callFireforce, callAmbulance;
    String phoneNumber = null;

    public CallFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_call, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        callPolice = view.findViewById(R.id.btn_callpolice);
        callAmbulance = view.findViewById(R.id.btn_callambulance);
        callFireforce = view.findViewById(R.id.btn_callfireforce);

        callPolice.setOnClickListener(this);
        callAmbulance.setOnClickListener(this);
        callFireforce.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_callpolice:
                //dummy number
                phoneNumber = "0813080000";
                Intent callPoliceIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
                startActivity(callPoliceIntent);

                break;
            case R.id.btn_callambulance:

                phoneNumber = "0813080001";
                Intent callAmbulanceIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
                startActivity(callAmbulanceIntent);

                break;
            case R.id.btn_callfireforce:

                phoneNumber = "0813080002";
                Intent callFireforceIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
                startActivity(callFireforceIntent);

                break;

        }
    }
}
