package com.example.myflarebeta;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnKecelakaan;
    Button btnKebakaran;
    Button btnPublic,btnOfficer;
    Button btnLogout;

    private Boolean isLoggingOut = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        btnPublic = findViewById(R.id.btn_public);
        btnOfficer = findViewById(R.id.btn_officer);
        btnKebakaran = findViewById(R.id.btn_kebakaran);
        btnKecelakaan = findViewById(R.id.btn_kecelakaan);
        btnLogout =findViewById(R.id.btn_logout);

        btnKecelakaan.setOnClickListener(this);
        btnKebakaran.setOnClickListener(this);
        btnOfficer.setOnClickListener(this);
        btnPublic.setOnClickListener(this);


        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(this, "Membutuhkan Izin Lokasi", Toast.LENGTH_SHORT).show();
            } else {

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        1);
            }
        } else {
            // Permission has already been granted
            Toast.makeText(this, "Izin Lokasi diberikan", Toast.LENGTH_SHORT).show();
        }


        btnLogout = (Button) findViewById(R.id.btn_logout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLoggingOut = true;


                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(MainActivity.this, LoginOfficerActivity.class);
                startActivity(intent);
                finish();
                return;
            }
        });
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (!isLoggingOut){
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_kecelakaan:
                Intent mIntent= new Intent(MainActivity.this,MapsActivity.class);
                startActivity(mIntent);
                break;
            case R.id.btn_kebakaran:
                Intent i =new Intent(MainActivity.this,MapsActivity.class);
                startActivity(i);
                break;

            case R.id.btn_officer:
                Intent officer=new Intent(MainActivity.this,LoginOfficerActivity.class);
                startActivity(officer);

                break;

            case R.id.btn_logout:
                Intent Logout=new Intent(MainActivity.this,LoginOfficerActivity.class);
                startActivity(Logout);;
                break;

        }
    }
}
